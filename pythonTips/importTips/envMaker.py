import sys
import os 

# pythonpath.confのあるディレクトリをhomeとする
path_conf = sys.argv[1]
env_home = os.path.abspath(os.path.dirname(path_conf))

# 書き込みたい相対pathの一覧を取得
path_list = []
with open(path_conf) as f:
	for line in f.readlines():
		path_list.append(os.path.join(env_home, line.rstrip()))

# .envファイルを作成
env_path = os.path.join(env_home, '.env')
with open(env_path, 'w') as f:
	paths = ';'.join(path_list)
	f.write(f'PYTHONPATH={paths}\n')