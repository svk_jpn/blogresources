# ModuleNotFoundError 対策用サンプル

## 1.構成

|要素|説明|
|:-|:-|
|workspace|ModuleNotFoundErrorが起きるようなサンプルプロジェクト|
|activate.ps1|.envを取り込むために修正したactivate.ps1|
|pythonpath.conf|.envのPYTHONPATHに追加する相対パスリスト。|
|envMaker.py|pythonpath.confから.envファイルを生成するスクリプト。|
